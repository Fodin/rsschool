import './styles.scss';
import './favicon.ico';
import './assets/images/0.jpg';
import './assets/images/1.jpg';
import './assets/images/2.jpg';
import './assets/images/3.jpg';
import './assets/images/4.jpg';
import './assets/images/5.jpg';
import './assets/images/6.jpg';
import './assets/images/7.jpg';
import './assets/images/8.jpg';
import './assets/images/9.jpg';
import './assets/images/10.jpg';
import './assets/images/11.jpg';
import './assets/images/12.jpg';
import './assets/images/13.jpg';
import './assets/images/bg1.jpg';
import './assets/images/bg2.jpg';
import './assets/images/bg3.jpg';
import './assets/images/bg4.jpg';

import Dashboard from './dashboard';

window.onload = () => {
  const dash = new Dashboard();
  dash.newGame();
};
