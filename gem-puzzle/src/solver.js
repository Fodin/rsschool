/* eslint-disable no-use-before-define */
import { cyrb53, getIndexOfMin } from './gameutils';

export default class Solver {
  constructor(position, size) {
    this.size = size;

    this.positions = [position.map((x) => String.fromCharCode(x + 32)).join``];
    this.hashes = [cyrb53(this.positions[0])];
    this.scores = [this.getPositionScore(this.positions[0])];
    this.moves = [''];

    this.solutionPositions = [];
    this.solutionHashes = [];
  }

  solve() {
    let solution = '';

    while (this.scores[0] > 0) {
      this.addNewPositions();

      let indexOfMinScore = getIndexOfMin(this.scores);

      if (indexOfMinScore === 0 && this.moves.slice(-1)[0].length === 11) {
        indexOfMinScore = getIndexOfMin(this.scores, 1);
      }

      if (indexOfMinScore !== 0) {
        this.addSolutionPositions(this.moves[indexOfMinScore]);
        this.positions = [this.positions[indexOfMinScore]];
        this.hashes = [this.hashes[indexOfMinScore]];
        this.scores = [this.scores[indexOfMinScore]];
        solution = solution.concat(this.moves[indexOfMinScore]);
        this.moves = [''];
        log(this.positions[0], this.size);
        // eslint-disable-next-line no-console
        console.log('Score changed to ', this.scores[0]);
      }
    }

    return solution;
  }

  addSolutionPositions(moves) {
    let m = moves;
    while (m !== '') {
      const curPos = this.moves.indexOf(m);
      this.solutionPositions.push(this.positions[curPos]);
      this.solutionHashes.push(this.hashes[curPos]);
      m = m.slice(0, -1);
    }
  }

  addNewPositions() {
    const treeLevel = this.moves.slice(-1)[0].length;
    const currentBestScore = this.scores[0];
    let newBestScoreFound = false;

    const swapTiles = (str, first, second) => str.substring(0, first)
            + str.substr(second, 1) + str.substring(first + 1, second)
            + str.substr(first, 1) + str.substring(second + 1);

    for (let i = 0; i < this.moves.length; i += 1) {
      if (this.moves[i].length > treeLevel) break;
      if (newBestScoreFound) break;

      if (this.moves[i].length === treeLevel) {
        // Get all possible moves around blank tile
        const curPos = this.positions[i];
        const blankPos = curPos.indexOf(' ');
        const possibleMoves = [];
        if (blankPos % this.size !== 0) possibleMoves.push('r');
        if (blankPos % this.size !== this.size - 1) possibleMoves.push('l');
        if (Math.floor(blankPos / this.size) !== 0) possibleMoves.push('d');
        if (Math.floor(blankPos / this.size) !== this.size - 1) possibleMoves.push('u');

        // eslint-disable-next-line no-loop-func
        possibleMoves.forEach((dir) => {
          let position;
          // Generate position
          switch (dir) {
            case 'r':
              position = swapTiles(curPos, blankPos - 1, blankPos);
              break;
            case 'l':
              position = swapTiles(curPos, blankPos, blankPos + 1);
              break;
            case 'd':
              position = swapTiles(curPos, blankPos - this.size, blankPos);
              break;
            case 'u':
              position = swapTiles(curPos, blankPos, blankPos + this.size);
              break;
            default: break; // Still hate ESLint
          }
          // Generate hash and check position for unique
          const hash = cyrb53(position);

          const indexOfHash = this.solutionHashes.indexOf(hash); // Search for hash in positions
          const isPositionNew = !(indexOfHash !== -1
            && this.solutionPositions[indexOfHash] === position);
          if (isPositionNew) { // Add to the positions array
            this.positions.push(position);
            this.hashes.push(hash);
            this.scores.push(this.getPositionScore(position));
            this.moves.push(this.moves[i] + dir);

            if (this.scores.slice(-1) < currentBestScore) newBestScoreFound = true;
          }
        });
      }
    }
  }

  getPositionScore(position) {
    const { size } = this;
    let score = position.split``.reduce((acc, ch, pos) => {
      if (ch === ' ') return acc;
      const charPos = ch.charCodeAt(0) - 32 - 1;

      return acc // Compute Manhattan distance
        + Math.abs(Math.floor(charPos / size) - Math.floor(pos / size)) // y distance
        + Math.abs((charPos % size) - (pos % size)); // x distance
    }, 0);
    const p = position.split``.map((ch) => ch.charCodeAt(0) - 32);
    // Fines for corners neighbours
    // TL
    if (p[0] !== 1) {
      if (p[1] === 2) score += 2;
      if (p[size] === size + 1) score += 2;
    }
    // TR
    if (p[size - 1] !== size) {
      if (p[size - 2] === size - 1) score += 2;
      if (p[size * 2 - 1] === size * 2) score += 2;
    }
    // BL
    if (p[size * (size - 1)] !== size * (size - 1) + 1) {
      if (p[size * (size - 2)] === size * (size - 2) + 1) score += 2;
      if (p[size * (size - 1) + 1] === size * (size - 1) + 2) score += 2;
    }

    // Last move 1
    const isLastMove1 = Math.floor(p.indexOf([size * size - 1]) / size) === size - 1;
    const isLastMove2 = p.indexOf(size * (size - 1)) % size === size - 1;
    if (!isLastMove1 && !isLastMove2) score += 2;

    return score;
  }
}

function log(x, size) {
  const c = x.split``.map((ch) => ch.charCodeAt(0) - 32);
  // eslint-disable-next-line no-console
  console.log(c.reduce((str, num, index) => (str + num.toString().padStart(3, ' ') + (((index + 1) % size === 0) ? '\n' : '')), ''));
}
