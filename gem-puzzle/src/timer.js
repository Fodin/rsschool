export default class Timer {
  constructor(element) {
    this.timerId = 0;
    this.paused = true;
    this.valueTens = 0;
    this.element = element;
  }

  start() {
    if (!this.paused) return;

    this.paused = false;
    this.timerId = setInterval(this.process.bind(this), 100);
  }

  stop() {
    if (this.paused) return;

    this.paused = true;
    clearInterval(this.timerId);
  }

  reset() {
    this.valueTens = 0;
    this.paused = true;
    this.element.textContent = this.getFormattedTime();
  }

  process() {
    this.valueTens += 1;

    if (this.valueTens % 10 !== 0) return;

    this.element.textContent = this.getFormattedTime();
  }

  getFormattedTime() {
    const minutes = Math.floor(this.valueTens / 600).toString().padStart(2, '0');
    const seconds = Math.floor((this.valueTens / 10) % 60).toString().padStart(2, '0');

    return `${minutes}:${seconds}`;
  }
}
