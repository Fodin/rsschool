function validatePosition(pos, size) {
  let inversions = 0;
  let rowWithBlank;
  for (let i = 0; i < pos.length - 1; i += 1) {
    if (pos[i] !== 0) {
      for (let j = i + 1; j < pos.length; j += 1) {
        if (pos[j] !== 0 && pos[j] < pos[i]) {
          inversions += 1;
        }
      }
    } else {
      rowWithBlank = Math.floor(i / size) + 1;
    }
  }

  if (size % 2 !== 0) {
    return inversions % 2 === 0;
  }
  return (inversions + rowWithBlank) % 2 === 0;
}

/* eslint-disable no-param-reassign */
function shuffle(array) {
  for (let i = array.length - 1; i > 0; i -= 1) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

function getRandomArray(size) {
  const arr = Array(size).fill(0).map((el, i) => i);
  shuffle(arr);
  return arr;
}

function generatePosition(size) {
  let pos;

  do {
    pos = getRandomArray(size ** 2);
  } while (validatePosition(pos, size) === false);

  return pos;
}

function getIndexOfMin(arr, index = 0) { // Returns min of array
  let min = arr[index];
  const { length } = arr;

  for (let i = index; i < length; i += 1) {
    if (arr[i] < min) {
      min = arr[i];
    }
  }

  return arr.indexOf(min, index);
}

/* eslint-disable no-bitwise */
function cyrb53(str) {
  const p1 = 2654435761;
  const p2 = 1597334677;
  let h1 = 0xdeadbeef | 0;
  let h2 = 0x41c6ce57 | 0;
  for (let i = 0; i < str.length; i += 1) {
    const ch = str.charCodeAt(i);
    h1 = Math.imul(h1 + ch, p1);
    h2 = Math.imul(h2 + ch, p2);
  }
  h1 = Math.imul(h1 ^ h1 >>> 16, p2);
  h2 = Math.imul(h2 ^ h2 >>> 15, p1);
  return (h2 & 2097151) * 4294967296 + h1;
}

export {
  generatePosition, cyrb53, getIndexOfMin,
};
