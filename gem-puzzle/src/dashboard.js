/* eslint-disable no-alert */
import Game from './game';
import Sound from './sound';
import Timer from './timer';
import Highscore from './highscore';
import Storage from './storage';

export default class Dashboard {
  constructor() {
    this.numbersVisible = true;
    this.imagesVisible = true;
    this.gamePaused = true;
    this.autoSolve = false;

    this.game = undefined;
    this.sound = new Sound();
    this.timer = undefined;
    this.highscore = new Highscore();

    this.createHTML();

    this.timer = new Timer(document.querySelector('#dashboard_time'));
  }

  processEvent(eventName, params = {}) {
    const moveEl = document.querySelector('#dashboard_moves');
    let moves = +moveEl.textContent;
    switch (eventName) {
      case 'move':
        this.sound.tileMoveSound();
        this.timer.start();
        this.gamePaused = false;
        document.querySelectorAll('button')[1].classList.toggle('button_on', false);

        if (this.autoSolve) {
          moves -= 1;
        } else {
          moves += 1;
        }
        moveEl.textContent = moves.toString();
        break;
      case 'autoSolveStarted':
        this.autoSolve = true;
        moveEl.textContent = params.moves;
        this.timer.reset();
        break;
      case 'autoSolveCompleted':
        this.autoSolve = false;
        this.timer.stop();
        break;
      case 'win':
        this.timer.stop();
        alert(`Ура! Вы решили головоломку за ${this.timer.getFormattedTime()} и ${moves} ходов`);
        this.highscore.save(this.game.size, moves, this.timer.getFormattedTime());
        break;
      default: break;
    }
  }

  newGame() {
    const fieldParams = {
      size: +document.querySelector('#dashboard_range').value,
      numbersVisible: this.numbersVisible,
      imagesVisible: this.imagesVisible,
    };
    this.game = new Game(fieldParams, this.processEvent.bind(this));
    this.timer.reset();
    document.querySelector('#dashboard_moves').textContent = '0';
  }

  createHTML() {
    const buttons = [
      ['Start a new game', 'New game', (e) => this.newGame(e), false],
      ['Pause the current game and show the best scores', 'Pause', (e) => this.pauseGame(e), true],
      ['Solve the puzzle', 'Solve', (e) => this.solveGame(e), false],
      ['Load previously saved game', 'Load', (e) => this.loadGame(e), false],
      ['Save current game', 'Save', (e) => this.saveGame(e), false],
      ['Show numbers on the tiles', 'Numbers', (e) => this.showNumbers(e), true],
      ['Show image on tiles', 'Image', (e) => this.showBGs(e), true],
      ['Turn sound on/off', 'Sound', (e) => this.switchSound(e), true],
      ['Show hall of fame', 'Highscore', (e) => this.showHighScore(e), false],
    ];

    const body = document.querySelector('body');
    const element = document.createElement('div');
    element.className = 'dashboard';

    // Create buttons
    buttons.forEach((el) => {
      element.insertAdjacentHTML('beforeend',
        `<button class="dashboard_button${el[3] ? ' button_on' : ''}" title="${el[0]}">${el[1]}</button>`);
    });

    // Create range input
    element.querySelector('button:nth-child(1)').insertAdjacentHTML('afterend',
      `<input id="dashboard_range" type="range" value="4" min="3" max="8" 
                list="dashboard_range_sizes" title="Select size of the field"><datalist 
                id="dashboard_range_sizes">
              <option value="3"><option value="4"><option value="5">
              <option value="6"><option value="7"> <option value="8">
            </datalist>`);

    // Create displays
    element.insertAdjacentHTML('beforeend', '<div class="dashboard_display" >Time: <span id="dashboard_time">00:00</span></div>');
    element.insertAdjacentHTML('beforeend', '<div class="dashboard_display" > Moves: <span id="dashboard_moves">0</span></div>');

    // Wrap elements into couples
    const elements = element.querySelectorAll('button, input, .dashboard_display');
    for (let i = 0; i < elements.length; i += 2) {
      const wrapper = document.createElement('div');
      wrapper.className = 'dashboard_wrapper';
      wrapper.append(elements[i]);
      wrapper.append(elements[i + 1]);
      element.append(wrapper);
    }

    body.append(element);

    // Add eventlisteners
    document.querySelectorAll('button.dashboard_button').forEach((btn, i) => {
      btn.addEventListener('click', buttons[i][2]);
    });
    document.querySelector('#dashboard_range').addEventListener('change', () => this.newGame());
  }

  pauseGame(e) {
    this.gamePaused = !this.gamePaused;
    e.target.classList.toggle('button_on', this.gamePaused);

    if (this.gamePaused) {
      this.timer.stop();
      return;
    }

    this.timer.start();
  }

  saveGame() {
    const fieldParams = {
      size: +document.querySelector('#dashboard_range').value,
      numbersVisible: this.numbersVisible,
      imagesVisible: this.imagesVisible,
      position: this.game.position,
      timerTens: this.timer.valueTens,
      moves: +document.querySelector('#dashboard_moves').textContent,
      spoiledByAutoSolve: this.game.spoiledByAutoSolve,
    };

    Storage.save('save', fieldParams);
    alert('The game has been saved');
  }

  loadGame() {
    const fieldParams = Storage.load('save');

    if (fieldParams === null) {
      alert('There are no saved games.');
      return;
    }

    this.game = new Game(fieldParams, this.processEvent.bind(this));
    this.game.spoiledByAutoSolve = fieldParams.spoiledByAutoSolve;

    this.timer.valueTens = fieldParams.timerTens;
    this.timer.stop();
    this.game.paused = true;
    document.querySelector('#dashboard_time').textContent = this.timer.getFormattedTime();

    document.querySelector('#dashboard_moves').textContent = fieldParams.moves;

    document.querySelector('#dashboard_range').value = fieldParams.size;
  }

  showHighScore() {
    alert(this.highscore.getTextHighscore(this.game.size));
  }

  showNumbers(e) {
    this.numbersVisible = !this.numbersVisible;
    e.target.classList.toggle('button_on', this.numbersVisible);
    document.querySelector('.field').classList.toggle('field-show-text', this.numbersVisible);
  }

  solveGame() {
    this.game.solve();
  }

  showBGs(e) {
    this.imagesVisible = !this.imagesVisible;
    e.target.classList.toggle('button_on', this.imagesVisible);
    document.querySelector('.field').classList.toggle('field-show-bg', this.imagesVisible);
  }

  switchSound(e) {
    this.sound.switchSound();
    e.target.classList.toggle('button_on', !this.sound.mute);
  }
}
