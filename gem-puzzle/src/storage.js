const STORAGE_PREFIX = 'Fodin';

export default class Storage {
  static save(key, value) {
    localStorage.setItem(`${STORAGE_PREFIX}-${key}`, JSON.stringify(value));
  }

  static load(key) {
    const rawValue = localStorage.getItem(`${STORAGE_PREFIX}-${key}`);

    if (rawValue === null) return null;

    return JSON.parse(rawValue);
  }
}
