import move1 from './assets/audio/move1.mp3';
import move2 from './assets/audio/move2.mp3';
import move3 from './assets/audio/move3.mp3';
import move4 from './assets/audio/move4.mp3';
import move5 from './assets/audio/move5.mp3';
import move6 from './assets/audio/move6.mp3';

export default class Sound {
  constructor() {
    this.mute = false;
    this.previousMoveSound = move1;
    this.sounds = [move1, move2, move3, move4, move5, move6];
  }

  tileMoveSound() {
    let trySound;

    if (this.mute) return;

    do {
      trySound = Math.round(Math.random() * 6 - 0.5);
    } while (this.previousMoveSound === this.sounds[trySound]);

    this.previousMoveSound = this.sounds[trySound];
    new Audio(this.sounds[trySound]).play().then((r) => r);
  }

  switchSound() {
    this.mute = !this.mute;
  }
}
