import Storage from './storage';

export default class Highscore {
  constructor() {
    this.table = undefined;

    if (Storage.load('highscore') === null) {
      this.table = new Array(6).fill([]);
      Storage.save('highscore', this.table);
    }

    this.table = Storage.load('highscore');
  }

  save(fieldSize, moves, timespent) {
    const sizeIndex = fieldSize - 3;
    const curDatetime = new Date();

    this.table[sizeIndex].push({
      datetime: `${curDatetime.toLocaleDateString('ru')} ${curDatetime.toLocaleTimeString('ru')}`,
      moves,
      timespent,
    });
    this.table[sizeIndex].sort((a, b) => a.moves - b.moves);

    if (this.table[sizeIndex].length > 10) this.table[sizeIndex].length = 10;

    Storage.save('highscore', this.table);
  }

  getTextHighscore(fieldSize) {
    const sizeIndex = fieldSize - 3;

    if (this.table[sizeIndex].length === 0) {
      return 'There are no highsores for this field size yet.';
    }

    const highscore = [`Hall of fame for fieldsize ${fieldSize}x${fieldSize}`];

    this.table[sizeIndex].forEach((record, i) => {
      highscore.push(`${(i + 1).toString().padStart(2, ' ')}. ${record.datetime} moves: ${record.moves} time: ${record.timespent}`);
    });

    return highscore.join`\n`;
  }
}
