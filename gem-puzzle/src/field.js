const MOUSE_CLICK_THRESHOLD = 3;
const DRAG_UNDO_DISTANCE = 0.3;

export default class Field {
  constructor({
    size, position, numbersVisible, imagesVisible,
  }, gameEvent) {
    this.size = size;
    this.position = position;
    this.gameEvent = gameEvent;

    this.createField(numbersVisible, imagesVisible);
    this.addEventListeners();
  }

  getMoveDirection(tileNumber) {
    const blankPos = this.position.indexOf(0);
    const tilePos = this.position.indexOf(+tileNumber);

    switch (tilePos - blankPos) {
      case 1:
        return 'left';
      case -1:
        return 'right';
      case this.size:
        return 'up';
      case -this.size:
        return 'down';
      default:
        return '';
    }
  }

  createField(numbersVisible, imagesVisible) {
    const field = document.querySelector('.field');
    if (field) {
      field.remove();
    }
    const element = document.createElement('div');

    element.className = 'field';
    element.classList.toggle('field-show-text', numbersVisible);
    element.classList.toggle('field-show-bg', imagesVisible);
    element.style = `--size: ${this.size}; --bgimg: url("./${Math.round(Math.random() * 14 - 0.5).toString()}.jpg")`;

    for (let i = 0; i < this.size ** 2; i += 1) {
      const curPos = this.position[i];

      if (curPos) {
        element.insertAdjacentHTML('beforeend', `<div class="tile" 
            style="--bgcol: ${(curPos - 1) % this.size}; --bgrow: ${Math.floor((curPos - 1) / this.size)}">${curPos}</div>`);
      } else {
        element.insertAdjacentHTML('beforeend', '<div class="tile blank">0</div>');
      }
    }

    document.querySelector('body').append(element);
  }

  addEventListeners() {
    const body = document.querySelector('body');
    const pos = this.position;
    body.querySelectorAll('.tile:not(.blank)').forEach((el) => {
      el.addEventListener('mousedown', (e) => {
        const startX = e.pageX;
        const startY = e.pageY;
        const tileEl = e.target;
        const tileSize = document.querySelector('.field').computedStyleMap().get('width').value / this.size;
        const coeffWidth = tileSize / tileEl.getBoundingClientRect().width;
        const coeffHeight = tileSize / tileEl.getBoundingClientRect().height;
        const moveDirection = this.getMoveDirection(tileEl.textContent);

        if (!moveDirection) { // There are no moves
          return;
        }

        tileEl.style.zIndex = '10';
        tileEl.classList.add('dragging');

        const onMouseMove = (ev) => {
          switch (moveDirection) {
            case 'left':
              tileEl.style.left = `${Math.min(Math.max(-tileSize, (ev.pageX - startX) * coeffWidth), 0)}px`;
              break;
            case 'right':
              tileEl.style.left = `${Math.max(Math.min(tileSize, (ev.pageX - startX) * coeffWidth), 0)}px`;
              break;
            case 'up':
              tileEl.style.top = `${Math.min(Math.max(-tileSize, (ev.pageY - startY) * coeffHeight), 0)}px`;
              break;
            case 'down':
              tileEl.style.top = `${Math.max(Math.min(tileSize, (ev.pageY - startY) * coeffHeight), 0)}px`;
              break;
            default: break; // Sometimes I hate ESLint
          }
        };

        const swapTiles = () => {
          tileEl.removeEventListener('transitionend', swapTiles);
          let nodes = document.querySelectorAll('.tile');
          let firstElPos = pos.indexOf(0);
          let secondElPos = pos.indexOf(+tileEl.textContent);
          if (firstElPos > secondElPos) {
            [firstElPos, secondElPos] = [secondElPos, firstElPos];
          }
          tileEl.style.top = '0';
          tileEl.style.left = '0';

          nodes[firstElPos].before(nodes[secondElPos]);
          nodes = document.querySelectorAll('.tile');
          nodes[secondElPos].after(nodes[firstElPos + 1]);
        };

        const onMouseUp = (ev) => {
          tileEl.style.zIndex = '';
          tileEl.classList.remove('dragging');
          document.removeEventListener('mousemove', onMouseMove);
          document.removeEventListener('mouseup', onMouseUp);

          const isDrag = (Math.abs(ev.pageX - startX) > MOUSE_CLICK_THRESHOLD)
            || (Math.abs(ev.pageY - startY) > MOUSE_CLICK_THRESHOLD);
          const isDragUndo = isDrag
            && (Math.abs(+tileEl.style.left.replace('px', '')) < tileSize * DRAG_UNDO_DISTANCE)
            && (Math.abs(+tileEl.style.top.replace('px', '')) < tileSize * DRAG_UNDO_DISTANCE);

          if (isDragUndo) {
            tileEl.style.left = '0px';
            tileEl.style.top = '0px';
          } else {
            switch (moveDirection) {
              case 'left':
                tileEl.style.left = `${-tileSize}px`;
                break;
              case 'right':
                tileEl.style.left = `${tileSize}px`;
                break;
              case 'up':
                tileEl.style.top = `${-tileSize}px`;
                break;
              case 'down':
                tileEl.style.top = `${tileSize}px`;
                break;
              default: break;
            }

            tileEl.addEventListener('transitionend', swapTiles);
            this.gameEvent('move', { move: moveDirection });
          }
        };

        document.addEventListener('mousemove', onMouseMove);
        document.addEventListener('mouseup', onMouseUp);
      });
    });
  }
}
