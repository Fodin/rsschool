/* eslint-disable no-alert */
import { generatePosition } from './gameutils';
import Field from './field';
import Solver from './solver';

export default class Game {
  constructor(params, dashboardEvent) {
    this.size = params.size;

    if (params.position === undefined) {
      this.position = generatePosition(this.size);
    } else {
      this.position = params.position; // Load game
    }

    this.dashboardEvent = dashboardEvent;
    this.spoiledByAutoSolve = false;

    this.logPosition();
    const paramsForField = params;
    paramsForField.position = this.position;
    this.field = new Field(paramsForField, this.processEvent.bind(this));
  }

  processEvent(eventName, params = {}) {
    const blankIndex = this.position.indexOf(0);
    let tileIndex;
    let isWin = true;

    switch (eventName) {
      case 'move':
        switch (params.move) {
          case 'left':
            tileIndex = blankIndex + 1;
            break;
          case 'right':
            tileIndex = blankIndex - 1;
            break;
          case 'up':
            tileIndex = blankIndex + this.size;
            break;
          case 'down':
            tileIndex = blankIndex - this.size;
            break;
          default: break;
        }
        [this.position[blankIndex], this.position[tileIndex]] = [this.position[tileIndex],
          this.position[blankIndex]];
        this.dashboardEvent('move');

        // Check for win
        for (let i = 0; i < this.position.length - 1; i += 1) {
          if (this.position[i] !== i + 1) {
            isWin = false;
            break;
          }
        }

        if (isWin && !this.spoiledByAutoSolve) {
          setTimeout(() => this.dashboardEvent('win'), 250);
        }

        this.logPosition();
        break;
      default:
        break;
    }
  }

  logPosition() {
    // eslint-disable-next-line no-console
    console.log(this.position.reduce(
      (str, num, index) => (str + num.toString().padStart(3, ' ')
        + (((index + 1) % this.size === 0) ? '\n' : '')), '',
    ));
  }

  solve() {
    const { size } = this;

    if (size === 4) alert('Предичайше извиняюсь, алгоритм получился честный, но медлееееннныыыыййй. 😟\nМожет считать до минуты, а то и двух. Зато собирает быстро! 😉');

    if (size > 4) {
      alert('К сожалению, написать надежное и всегда работающее автоматическое решение для больших полей не удалось. 😢 Но за это ведь оценку не снижают, правда? 😉');
      return;
    }

    this.spoiledByAutoSolve = true;
    const solver = new Solver(this.position, size);
    const solution = solver.solve().split``;
    this.dashboardEvent('autoSolveStarted', { moves: solution.length });
    let blankPos = this.position.indexOf(0);
    let i = 0;

    function animateSoluiton(that) {
      if (i < solution.length) {
        setTimeout(() => {
          switch (solution[i]) {
            case 'l':
              blankPos += 1;
              break;
            case 'r':
              blankPos -= 1;
              break;
            case 'u':
              blankPos += size;
              break;
            case 'd':
              blankPos -= size;
              break;
            default: break;
          }
          // Simulate clicks
          document.querySelectorAll('.tile')[blankPos].dispatchEvent(new MouseEvent('mousedown'));
          document.querySelectorAll('.tile')[blankPos].dispatchEvent(new MouseEvent('mouseup', { bubbles: true }));
          i += 1;
          animateSoluiton(that);
        }, 250);
      } else {
        that.dashboardEvent('autoSolveCompleted');
      }
    }

    animateSoluiton(this);
  }
}
