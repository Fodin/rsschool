const time = document.querySelector('time');
const date = document.querySelector('.date');
const greeting = document.querySelector('.greeting');
const name = document.querySelector('.name');
const focus = document.querySelector('.focus');
const city = document.querySelector('.city');
const reload = document.querySelector('.reload');
const weatherIcon = document.querySelector('.weather-icon');
const temperature = document.querySelector('.temperature');
const windSpeed = document.querySelector('.weather-windspeed');
const humidity = document.querySelector('.weather-humidity');
const weatherDescription = document.querySelector('.weather-description');
let currentImage = 0;
let lastHour;
const backgroundsArray = [];

function showTime() {
    const today = new Date();
    const currentHour = today.getHours();
    const min = today.getMinutes().toString().padStart(2, "0");
    const sec = today.getSeconds().toString().padStart(2, "0");

    if (lastHour !== currentHour) {
        loadBackground(currentHour);
        setGreetings();
        lastHour = currentHour;
    }

    date.innerHTML = today.toLocaleDateString("en-US",{weekday:"long", month:"long", day:"2-digit"});
    time.innerHTML = `${currentHour}<span>:</span>${min}<span>:</span>${sec}`;

    setTimeout(showTime, 1000);
}

function changeBackgroundImage() {
    currentImage = (currentImage + 1) % 24;
    loadBackground(currentImage);
}

function loadBackground(hour) {
    const body = document.querySelector('body');
    const translatedIndex = backgroundsArray[hour];
    const img = document.createElement('img');
    reload.style.display = "none";
    img.src = `assets/images/${translatedIndex}.jpg`;
    img.onload = () => {
        body.style.backgroundImage = `url("assets/images/${translatedIndex}.jpg")`;
        setTimeout(() => reload.style.display = "", 1000);
    };
}

function setGreetings() {
    const hour = new Date().getHours();

    if (hour < 6)        greeting.textContent = 'Good Night, ';
    else if (hour < 12)  greeting.textContent = 'Good Morning, ';
    else if (hour < 18)  greeting.textContent = 'Good Afternoon, ';
    else                 greeting.textContent = 'Good Evening, ';
}

async function getWeather() {
    const url = `https://api.openweathermap.org/data/2.5/weather?q=${city.textContent.trim()}&lang=en&appid=da2172d253f3f2f8636b1459c7a49df8&units=metric`;
    const res = await fetch(url);
    const data = await res.json();

    if (data.cod === 200) {
        document.querySelector('.weather-container').style.display = "";

        weatherIcon.className = 'weather-icon owf';
        weatherIcon.classList.add(`owf-${data.weather[0].id}`);
        temperature.textContent = `${data.main.temp.toFixed(1)}°C`;
        weatherDescription.textContent = data.weather[0].description;
        windSpeed.textContent = `wind: ${data.wind.speed.toFixed(0)} m/s`;
        humidity.textContent = `humidity: ${data.main.humidity.toFixed(0)}%`;

        setTimeout(getWeather, 1000 * 60 * 60);
    } else {
        city.textContent = "Invalid city name!";
        document.querySelector('.weather-container').style.display = "none";
    }
    document.querySelector('.weather').classList.add("show");
}

async function getQuote() {
  const res = await fetch("https://type.fit/api/quotes");
  const data = await res.json();

  let index = randomInteger(0, data.length);

  while (data[index].text.length > 60) {
      index = randomInteger(0, data.length);
  }

  document.querySelector('.dayphrase').textContent = data[index].text;
  document.querySelector('.dayphrase-author').textContent = data[index].author || "Someone";
  document.querySelector('.phrase-container').classList.add("show");
}

function generateBackgroundsArray () {
    [0,6,12,18].forEach((i) => {
        let tempArray = [];
        while (tempArray.length !== 6) {
            let candidateIndex = randomInteger(i, i + 5);
            if (!tempArray.includes(candidateIndex)) {
                tempArray.push(candidateIndex);
            }
        }
        backgroundsArray.push(...tempArray);
    });
    console.log(backgroundsArray);
}

function randomInteger(min, max) {
  return Math.round(min - 0.5 + Math.random() * (max - min + 1));
}

function setField(event) {
    if (event.type === "keydown" && event.key !== "Enter") {
        return;
    }

    if (event.target.textContent.trim().length === 0) {
        event.target.textContent = localStorage.getItem(event.target.className);
        event.target.blur();
        return;
    }

    localStorage.setItem(event.target.className, event.target.textContent);
    event.target.blur();
    if (event.target.className === "city" && event.type === "blur") {
        document.querySelector('.weather').classList.remove("show");
        setTimeout(() => getWeather(), 1000) ;
    }
}

function clearField(event) {
    setTimeout(()=>event.target.textContent = " ", 0);
}

function setEventListenerForEditable (field){
    field.addEventListener('focus', clearField);
    field.addEventListener('keydown', setField);
    field.addEventListener('blur', setField);
}

setEventListenerForEditable(focus);
setEventListenerForEditable(name);
setEventListenerForEditable(city);
reload.addEventListener("click", changeBackgroundImage);
document.querySelector('.reload-phrase').addEventListener("click", () => {
    document.querySelector('.phrase-container').classList.remove("show");
    setTimeout(() => getQuote(), 1000)});

localStorage.setItem("name", name.textContent = localStorage.getItem("name") || name.textContent);
localStorage.setItem("focus", focus.textContent = localStorage.getItem("focus") || focus.textContent);
localStorage.setItem("city", city.textContent = localStorage.getItem("city") || city.textContent);

generateBackgroundsArray();
showTime();
setGreetings();
getWeather();
getQuote();