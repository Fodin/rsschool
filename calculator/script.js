class Calculator {
  constructor (display){
    this.display = display;
    this.operations = {
      "+" : (a,b) => a + b,
      "-" : (a,b) => a - b,
      "/" : (a,b) => a / b,
      "*" : (a,b) => a * b,
      "**" : (a,b) => a ** b,
    };
    this.clear();
  }

  dispatchInput(input) {
    if (this.isError) {
      this.clear();
    }

    if (/^[0-9.]$/.test(input)) {
      this.inputNumber(input)
    } else if (/(AC|Escape)/.test(input)) {
      this.clear()
    } else if (/(DEL|Delete|Backspace)/.test(input)) {
      this.deleteLastDigit();
    } else if (/^(lg|1\/x|√|±)$/.test(input)) {
      this.unaryOperation(input);
    } else if (/^(÷|\/|×|\*|\+|-|xy|y|=|Enter)$/.test(input)) {
      this.binaryOperation(input.replace("÷", "/")
              .replace("×", "*")
              .replace(/(xy|y)/, "**")
              .replace("Enter", "="));
    }

    this.refreshDisplay();
  }

  formatNumber(number) {
    if (!isFinite(number)) {
      this.secondOperand = "Error";
      this.isError = true;
      return;
    }
    let baseLength = Math.abs(number).toFixed().toString().length;
    if (baseLength > DISPLAY_RESOLUTION) {
      this.secondOperand = "Overflow";
      this.isError = true;
      return;
    }

    const exponentialDivider = 10 ** (DISPLAY_RESOLUTION + baseLength - 3);
    if (Math.abs(number - Math.round(number)) < 1 / exponentialDivider){
      number = Math.round( number * exponentialDivider) / exponentialDivider;
    }

    this.secondOperand = (new Intl.NumberFormat('en-IN', { maximumSignificantDigits: DISPLAY_RESOLUTION })
        .format(number)).replace(/,/g, "");
  }

  computeBinary() {
    return this.operations[this.operation](+this.firstOperand, +this.secondOperand);
  }

  binaryOperation(operation) {
    if (operation === "=") {
      this.operationEqual();
      this.isResetNeeded = true;
      return;
    }

    if (this.operation === "") {
      this.firstOperand = this.secondOperand;
    } else {
      if (!this.isResetNeeded) {
        this.secondOperand = this.computeBinary()
        this.formatNumber(this.secondOperand);
      }
    }

    this.operation = operation;
    this.isResetNeeded = true;
    this.isRepeatPossible = false;
  }

  operationEqual(){
    if (this.isRepeatPossible) {
      this.firstOperand = this.secondOperand;
      this.secondOperand = this.lastOperand;
    }

    this.secondOperand = this.computeBinary()
    this.formatNumber(this.secondOperand);
    this.isRepeatPossible = true;
  }

  unaryOperation(operation) {
    switch (operation) {
      case "lg":
        this.formatNumber(Math.log10(+this.secondOperand));
        this.isResetNeeded = true;
        break;
      case "1/x":
        this.formatNumber(1/+this.secondOperand);
        this.isResetNeeded = true;
        break;
      case "√":
        this.formatNumber(Math.sqrt(+this.secondOperand));
        this.isResetNeeded = true;
        break;
      case "±":
        if (this.isResetNeeded) {
          this.secondOperand = "0";
        }

        if (this.secondOperand.includes("-")) {
          this.secondOperand = this.secondOperand.slice(1);
        } else {
          this.secondOperand = "-" + this.secondOperand;
        }

        this.isResetNeeded = false;
        break;
    }

    this.lastOperand = this.secondOperand;
  }

  inputNumber(number) {
    if (this.isResetNeeded) {
      if (this.operation !== "") {
        this.firstOperand = this.secondOperand;
      }
      this.secondOperand = "0";
      this.isResetNeeded = false;
    }

    const isOverflow = this.secondOperand.replace(/[-.]/g, "").length === DISPLAY_RESOLUTION;
    const isDotUnwanted = (number === ".") && this.secondOperand.includes(".");

    if (isOverflow || isDotUnwanted) {
      return;
    }

    if (number !== ".") {
      if (this.secondOperand === "0") {
        this.secondOperand = ''
      }
      if (this.secondOperand === "-0") {
        this.secondOperand = '-'
      }
    }

    this.secondOperand += number;
    this.lastOperand = this.secondOperand;
    if (this.isRepeatPossible) {
      this.isRepeatPossible = false;
      this.operation = "";
    }
  }

  deleteLastDigit() {
    if (this.isResetNeeded) {
      return;
    }

    if (this.secondOperand.replace("-", "").length > 1) {
      this.secondOperand = this.secondOperand.slice(0,-1);
    } else {
      this.secondOperand = "0";
    }

    this.lastOperand = this.secondOperand;
  }

  refreshDisplay() {
    this.display.innerText = ""
    setTimeout(() => this.display.innerText = this.secondOperand, 50);
  }

  clear() {
    this.firstOperand = '';
    this.secondOperand = '0';
    this.lastOperand = '0';
    this.operation = '';
    this.isError = false;
    this.isResetNeeded = false;
    this.isRepeatPossible = false;
  }
}

const DISPLAY_RESOLUTION = 10;
const calc = new Calculator(document.querySelector(".output"));

for (let button of document.getElementsByTagName("button")) {
  button.addEventListener("click", () => calc.dispatchInput(button.innerText));
}

document.addEventListener("keydown", (e) => calc.dispatchInput(e.key));