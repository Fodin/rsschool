const petsData = [
  {
    "name": "Jennifer",
    "img": "../../assets/images/pets-jennifer.jpg",
    "type": "Dog",
    "breed": "Labrador",
    "description": "Jennifer is a sweet 2 months old Labrador that is patiently waiting to find a new forever home. This girl really enjoys being able to go outside to run and play, but won't hesitate to play up a storm in the house if she has all of her favorite toys.",
    "age": "2 months",
    "inoculations": ["none"],
    "diseases": ["none"],
    "parasites": ["none"]
  },
  {
    "name": "Sophia",
    "img": "../../assets/images/pets-sophia.jpg",
    "type": "Dog",
    "breed": "Shih tzu",
    "description": "Sophia here and I'm looking for my forever home to live out the best years of my life. I am full of energy. Everyday I'm learning new things, like how to walk on a leash, go potty outside, bark and play with toys and I still need some practice.",
    "age": "1 month",
    "inoculations": ["parvovirus"],
    "diseases": ["none"],
    "parasites": ["none"]
  },
  {
    "name": "Woody",
    "img": "../../assets/images/pets-woody.jpg",
    "type": "Dog",
    "breed": "Golden Retriever",
    "description": "Woody is a handsome 3 1/2 year old boy. Woody does know basic commands and is a smart pup. Since he is on the stronger side, he will learn a lot from your training. Woody will be happier when he finds a new family that can spend a lot of time with him.",
    "age": "3 years 6 months",
    "inoculations": ["adenovirus", "distemper"],
    "diseases": ["right back leg mobility reduced"],
    "parasites": ["none"]
  },
  {
    "name": "Scarlett",
    "img": "../../assets/images/pets-scarlet.jpg",
    "type": "Dog",
    "breed": "Jack Russell Terrier",
    "description": "Scarlett is a happy, playful girl who will make you laugh and smile. She forms a bond quickly and will make a loyal companion and a wonderful family dog or a good companion for a single individual too since she likes to hang out and be with her human.",
    "age": "3 months",
    "inoculations": ["parainfluenza"],
    "diseases": ["none"],
    "parasites": ["none"]
  },
  {
    "name": "Katrine",
    "img": "../../assets/images/pets-katrine.jpg",
    "type": "Cat",
    "breed": "British Shorthair",
    "description": "Katrine is a beautiful girl. She is as soft as the finest velvet with a thick lush fur. Will love you until the last breath she takes as long as you are the one. She is picky about her affection. She loves cuddles and to stretch into your hands for a deeper relaxations.",
    "age": "6 months",
    "inoculations": ["panleukopenia"],
    "diseases": ["none"],
    "parasites": ["none"]
  },
  {
    "name": "Timmy",
    "img": "../../assets/images/pets-timmy.jpg",
    "type": "Cat",
    "breed": "British Shorthair",
    "description": "Timmy is an adorable grey british shorthair male. He loves to play and snuggle. He is neutered and up to date on age appropriate vaccinations. He can be chatty and enjoys being held. Timmy has a lot to say and wants a person to share his thoughts with.",
    "age": "2 years 3 months",
    "inoculations": ["calicivirus", "viral rhinotracheitis"],
    "diseases": ["kidney stones"],
    "parasites": ["none"]
  },
  {
    "name": "Freddie",
    "img": "../../assets/images/pets-freddie.jpg",
    "type": "Cat",
    "breed": "British Shorthair",
    "description": "Freddie is a little shy at first, but very sweet when he warms up. He likes playing with shoe strings and bottle caps. He is quick to learn the rhythms of his human’s daily life. Freddie has bounced around a lot in his life, and is looking to find his forever home.",
    "age": "2 months",
    "inoculations": ["rabies"],
    "diseases": ["none"],
    "parasites": ["none"]
  },
  {
    "name": "Charly",
    "img": "../../assets/images/pets-charly.jpg",
    "type": "Dog",
    "breed": "Jack Russell Terrier",
    "description": "This cute boy, Charly, is three years old and he likes adults and kids. He isn’t fond of many other dogs, so he might do best in a single dog home. Charly has lots of energy, and loves to run and play. We think a fenced yard would make him very happy.",
    "age": "8 years",
    "inoculations": ["bordetella bronchiseptica", "leptospirosis"],
    "diseases": ["deafness", "blindness"],
    "parasites": ["lice", "fleas"]
  }
]

// Sliding mobile menu
function switchScroll() {
    document.body.style.overflow = document.getElementById("burger").checked ? "hidden" : "";
}

function closeMenu() {
    document.getElementById("burger").checked = false;
    switchScroll();
}

// Modal window
function modalOpen(){
  const index = this.dataset.petindex;
  const modal = document.querySelector('.modal');
  const modalDescContainer = modal.querySelector(".modal-desc-container");

  // Fill data
  document.querySelector('.modal-photo img').setAttribute("src", petsData[index].img);
  document.querySelector('.modal-photo img').setAttribute("alt", petsData[index].type + " "  + petsData[index].breed);
  modalDescContainer.querySelector("h3").textContent = petsData[index].name;
  modalDescContainer.querySelector("h4").textContent = petsData[index].type + " - "  + petsData[index].breed;
  modalDescContainer.querySelector("p").textContent = petsData[index].description;

  modalDescContainer.querySelector(".pet-age").textContent = petsData[index].age;
  modalDescContainer.querySelector(".pet-inoculations").textContent = petsData[index].inoculations.join(", ");
  modalDescContainer.querySelector(".pet-diseases").textContent = petsData[index].diseases.join(", ");
  modalDescContainer.querySelector(".pet-parasites").textContent = petsData[index].parasites.join(", ");

  // Show window
  document.body.style.overflow = "hidden";
  modal.style.top = window.pageYOffset + 'px';
  modal.style.display = "flex";
}

function modalClose(e){
  // Пипец костыль. Убрать.
  if (e.target.className !== "modal" && e.target.className !== "button button-secondary modal-close") {
    return;
  }
  document.body.style.overflow = "";
  document.querySelector('.modal').style.display = "none";
}

function randomInteger(min, max) {
  return Math.round(min - 0.5 + Math.random() * (max - min + 1));
}

function slide(direction) {
  const panel = document.querySelector('.friends-panel');
  const oldCards = document.querySelectorAll('.friends-card');
  let cardArray = [];

  for (let card of document.querySelectorAll(".friends-card")) {
    cardArray.push(+card.dataset.petindex);
  }

  while (cardArray.length !== 6) {
    let candidateIndex = randomInteger(0, 7);
    if (!cardArray.includes(candidateIndex)) {
      cardArray.push(candidateIndex);
    }
  }

  [3, 4, 5].forEach(i => {
    const newItem = document.createElement("div");
    const cardNum = cardArray[i];
    newItem.className = "friends-card";
    newItem.dataset.petindex = cardNum;
    newItem.innerHTML = `<img src="${petsData[cardNum].img}"` +
        `alt="${petsData[cardNum].type + ' ' + petsData[cardNum].breed}"><h4>${petsData[cardNum].name}</h4>` +
        `<button class="button button-secondary">Learn more</button></div>`;
    if (direction) {
      panel.append(newItem);
    } else {
      panel.prepend(newItem);
    }
  });

  panel.classList.add(direction ? "friends-slide-left" : "friends-slide-right");

  panel.addEventListener('animationend', () => {
    oldCards.forEach((el) => {
      el.remove();
    });
    panel.classList.remove(direction ? "friends-slide-left" : "friends-slide-right");
    for (let card of document.querySelectorAll(".friends-card")) {
      card.addEventListener("click", modalOpen);
    }
  });
}

for (let card of document.querySelectorAll(".friends-card")) {
  card.addEventListener("click", modalOpen);
}

document.querySelector("#btn-left").addEventListener("click", () => slide(true));
document.querySelector("#btn-right").addEventListener("click", () => slide(false));