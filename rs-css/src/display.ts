import hljs from 'highlight.js';
// eslint-disable-next-line import/no-cycle
import { Events, EventFunc } from './game';

export default class Display {
  private readonly raiseEvent: EventFunc;
  private readonly root: HTMLElement;
  private tooltipClassName: string;

  constructor(eventFunction: EventFunc) {
    this.raiseEvent = eventFunction;
    this.root = document.querySelector('#display');
    this.tooltipClassName = '';

    // Set eventlistener
    this.root.addEventListener('mouseover', (ev) => {
      const result: RegExpMatchArray = (ev.target as HTMLElement).parentElement.className.match(/e\d+/);
      this.raiseEvent(Events.SWITCH_TOOLTIP, { class: result ? result[0] : '' });
    });
  }

  setHtml(levelHtml: string): void {
    this.root.innerHTML = levelHtml;
  }

  tooltip(className: string, tooltipText: string): void {
    if (className === this.tooltipClassName) {
      return;
    }

    this.tooltipClassName = className;
    const el: HTMLElement = this.root.querySelector('.tooltip');
    if (el) el.remove();

    const elHighlight: HTMLElement = this.root.querySelector('.highlight');
    if (elHighlight) elHighlight.classList.remove('highlight');

    if (className !== '') {
      // Add tooltip
      const tooltipEl: HTMLElement = document.createElement('div');
      tooltipEl.className = 'tooltip';
      tooltipEl.textContent = tooltipText;
      hljs.highlightBlock(tooltipEl);
      this.root.querySelector(`.${className}`).append(tooltipEl);

      // Add highlight
      this.root.querySelector(`.${className}`).classList.add('highlight');
    }
  }

  getSelectorResult(selector: string): string[] {
    const result = [];
    try {
      this.root.querySelectorAll(selector).forEach((el) => {
        const searchResult = el.className.match(/e(\d+)/);
        if (searchResult) {
          result.push(searchResult[1]);
        }
      });
    } catch (e) {
      return [];
    }
    return result;
  }

  animateWrong(selector: string): void {
    let elemsToShake: NodeListOf<HTMLElement>;
    try {
      elemsToShake = this.root.querySelectorAll(selector);
      if (elemsToShake.length === 0) {
        elemsToShake = document.querySelectorAll('#display');
      }
    } catch (e) {
      elemsToShake = document.querySelectorAll('#display');
    }
    elemsToShake.forEach((el) => {
      el.classList.add('shake');
    });
    setTimeout(() => document.querySelectorAll('.shake').forEach((el) => {
      el.classList.remove('shake');
    }), 850);
  }

  animateRight(selector: string): void {
    this.root.querySelectorAll(selector).forEach((el) => {
      el.classList.add('fly-out');
    });
  }
}
