import { levels, Levels } from './levels';

export default class LevelsStorage {
  public levelsNumber : number;
  private levels: Levels;

  constructor() {
    this.levelsNumber = Object.keys(levels).length;
    this.levels = levels;
  }

  getLevelDisplayHTML(level: number) {
    return this.levels[level].display;
  }

  getLevelHTML(level: number) {
    return this.levels[level].html;
  }

  getLevelDescription(level: number) {
    return this.levels[level].desc;
  }

  getHelp(level: number) {
    return this.levels[level].help;
  }

  getAnswer(level: number) {
    return this.levels[level].answer;
  }
}
