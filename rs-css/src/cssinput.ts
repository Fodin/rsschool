import CodeMirror from 'codemirror';
import 'codemirror/mode/css/css';
import './assets/styles/codemirror.css';
import '../node_modules/codemirror/addon/display/placeholder';

export default class CSSInput {
  private readonly raiseEvent: Function;
  private readonly root: HTMLElement;
  private readonly cm: CodeMirror;

  constructor(eventFunction: Function) {
    this.raiseEvent = eventFunction;
    this.root = document.querySelector('.cssinputs');

    this.cm = CodeMirror.fromTextArea(document.querySelector('#cssinput'), {
      lineWrapping: false,
      matchBrackets: true,
      scrollbarStyle: 'null',
      placeholder: 'Input CSS here...',
    });

    // Add eventListeners
    this.root.addEventListener('keyup', (ev) => {
      if (ev.key === 'Enter') {
        ev.stopPropagation();
        this.raiseEvent('checkAnswer', { answer: this.cm.getValue() });
        this.cm.setValue('');
      }
    }, { capture: true });
    this.root.querySelector('button').addEventListener('click', () => {
      this.raiseEvent('checkAnswer', { answer: this.cm.getValue() });
      this.cm.setValue('');
    });
  }

  setCss(levelDescription: string): void {
    this.root.querySelector('#task-description').innerHTML = levelDescription;
    this.cm.setValue('');
  }

  setAnswer(help: string, timeout: number): void {
    for (let i = 1; i < help.length + 1; i += 1) {
      setTimeout(() => this.cm.setValue(help.slice(0, i)), timeout * i);
    }
  }
}
