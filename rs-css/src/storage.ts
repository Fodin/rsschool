const STORAGE_PREFIX: string = 'Fodin';

export default class Storage {
  static save(key : string, value) {
    localStorage.setItem(`${STORAGE_PREFIX}-${key}`, JSON.stringify(value));
  }

  static load(key : string) {
    const rawValue = localStorage.getItem(`${STORAGE_PREFIX}-${key}`);

    if (rawValue === null) return null;

    return JSON.parse(rawValue);
  }
}
