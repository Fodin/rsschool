interface Level {
  display: string,
  answer: string,
  help: string,
  html: string,
  desc: string,
}

export type Levels = {[key: string]: Level};

export const levels: Levels = {
  1: {
    display: `<monkey class="jump e1"><img src="./assets/images/monkey.svg"></monkey>
      <monkey class="jump e2"><img src="./assets/images/monkey.svg"></monkey>`,
    answer: '1 2',
    help: 'monkey',
    html: '<div class="e1">&lt;monkey&gt;&lt;/monkey&gt;</div><div class="e2">&lt;monkey&gt;&lt;/monkey&gt;</div>',
    desc: 'Select all monkeys',
  },
  2: {
    display: `<monkey class="e1"><img src="./assets/images/monkey.svg"></monkey>
      <sheep class="jump e2"><img src="./assets/images/sheep.svg"></sheep>`,
    answer: '2',
    help: 'sheep',
    html: '<div class="e1">&lt;monkey&gt;&lt;/monkey&gt;</div><div class="e2">&lt;sheep&gt;&lt;/sheep&gt;</div>',
    desc: 'Select a sheep',
  },
  3: {
    display: `<monkey class="jump e1" id="rambo"><img src="./assets/images/rambomonkey.svg"></monkey>
      <monkey class="e2"><img src="./assets/images/monkey.svg"></monkey>`,
    answer: '1',
    help: '#rambo',
    html: '<div class="e1">&lt;monkey id="rambo"&gt;&lt;/monkey&gt;</div><div class="e2">&lt;monkey&gt;&lt;/monkey&gt;</div>',
    desc: 'Select Rambo monkey',
  },
  4: {
    display: `<cage class="e1"><img src="./assets/images/littlecage.svg">
      <monkey class="jump e2" style="position: absolute; bottom:0; left:7%;"><img src="./assets/images/monkey.svg"></monkey>
      </cage>
      <monkey class="e3"><img src="./assets/images/monkey.svg"></monkey>`,
    answer: '2',
    help: 'cage monkey',
    html: `<div class="e1">&lt;cage&gt;</div>
      <div class="e2 t1">&lt;monkey&gt;&lt;/monkey&gt;</div>
      <div class="e1">&lt;/cage&gt;</div>\
      <div class="e3">&lt;monkey&gt;&lt;/monkey&gt;</div>`,
    desc: 'Select monkey in the cage',
  },
  5: {
    display: `<monkey class="e1" id="rambo"><img src="./assets/images/rambomonkey.svg">
      <crown class="jump e2 t1"  style="position: absolute; top:-17%; left:19%;"><img src="./assets/images/crown.svg"></crown>
      </monkey>
      <monkey class="e3"><img src="./assets/images/monkey.svg">
      <crown class="e4 t1" style="position: absolute; top:-17%; left:19%;"><img src="./assets/images/crown.svg"></crown>
      </monkey>`,
    answer: '2',
    help: '#rambo crown',
    html: `<div class="e1" id="rambo">&lt;monkey id="rambo"&gt;</div>
      <div class="e2 t1">&lt;crown&gt;&lt;/crown&gt;</div>
      <div class="e1">&lt;/monkey&gt;</div>
      <div class="e3">&lt;monkey&gt;</div>
      <div class="e4 t1">&lt;crown&gt;&lt;/crown&gt;</div>
      <div class="e3">&lt;/monkey&gt;</div>`,
    desc: 'Select Rambo\'s crown<br>Hint: use #id selector',
  },
  6: {
    display: `<monkey class="jump e1 baby" style="width: 40px;"><img src="./assets/images/monkey.svg"></monkey>
        <monkey class="e2"><img src="./assets/images/monkey.svg"></monkey>`,
    answer: '1',
    help: '.baby',
    html: '<div class="e1">&lt;monkey class="baby"&gt;&lt;/monkey&gt;</div><div class="e2">&lt;monkey&gt;&lt;/monkey&gt;</div>',
    desc: 'Select baby monkey<br>Hint: use .class selector',
  },
  // клетка(бэбиманки), забор(манки), забор(бэбиманки), бэбиовца
  7: {
    display: `<cage class="e1"><img src="./assets/images/littlecage.svg">
      <monkey class="jump e2 baby" style="position: absolute; bottom:0; left:22%;"><img src="./assets/images/monkey.svg"></monkey>
      </cage>
      <fence class="e3"><img src="./assets/images/fence.svg">
      <monkey class="e4" style="position: absolute; bottom:0; left:7%;"><img src="./assets/images/monkey.svg"></monkey>
      </fence>
      <fence class="e5"><img src="./assets/images/fence.svg">
      <monkey class="jump e6 baby" style="position: absolute; bottom:0; left:22%;"><img src="./assets/images/monkey.svg"></monkey>
      </fence>
<sheep class="e7 baby"><img src="./assets/images/sheep.svg"></sheep>`,
    answer: '2 6',
    help: 'monkey.baby',
    html: `<div class="e1">&lt;cage&gt;</div>
      <div class="e2 t1">&lt;monkey class="baby"&gt;&lt;/monkey&gt;</div>
      <div class="e1">&lt;/cage&gt;</div>
      <div class="e3">&lt;fence&gt;</div>
      <div class="e4 t1">&lt;monkey&gt;&lt;/monkey&gt;</div>
      <div class="e3">&lt;/fence&gt;</div>
      <div class="e5">&lt;fence&gt;</div>
      <div class="e6 t1">&lt;monkey class="baby"&gt;&lt;/monkey&gt;</div>
      <div class="e5">&lt;/fence&gt;</div>
      <div class="e7">&lt;sheep class="baby"&gt;&lt;/sheep&gt;</div>`,
    desc: 'Select all baby monkeys.<br>Hint: use combination of tag and class selectors',
  },
  // клетка (бэбиманки), забор(манки), клетка, манки
  8: {
    display: `<cage class="jump e1"><img src="./assets/images/littlecage.svg">
      <monkey class="e2 baby" style="position: absolute; bottom:0; left:22%;"><img src="./assets/images/monkey.svg"></monkey></cage>
      <fence class="e3 jump"><img src="./assets/images/fence.svg">
      <monkey class="e4" style="position: absolute; bottom:0; left:7%;"><img src="./assets/images/monkey.svg"></monkey></fence>
      <cage class="e5 jump"><img src="./assets/images/littlecage.svg"></cage>
      <monkey class="e6"><img src="./assets/images/monkey.svg"></monkey>`,
    answer: '1 3 5',
    help: 'cage, fence',
    html: `<div class="e1">&lt;cage&gt;</div>
      <div class="e2 t1">&lt;monkey class="baby"&gt;&lt;/monkey&gt;</div>
      <div class="e1">&lt;/cage&gt;</div>
      <div class="e3">&lt;fence&gt;</div>
      <div class="e4 t1">&lt;monkey&gt;&lt;/monkey&gt;</div>
      <div class="e3">&lt;/fence&gt;</div>
      <div class="e5">&lt;cage&gt;&lt;/cage&gt;</div>
      <div class="e6">&lt;monkey&gt;&lt;/monkey&gt;</div>`,
    desc: 'Select all fences and cage.<br>Hint: use comma combinator',
  },
  // клетка(бэбиманки), забор(манки), забор(бэбиманки), бэбиовца
  9: {
    display: `<cage class="jump e1"><img src="./assets/images/littlecage.svg">
      <monkey class="jump e2 baby" style="position: absolute; bottom:0; left:22%;"><img src="./assets/images/monkey.svg"></monkey>
      </cage>
      <fence class="jump e3"><img src="./assets/images/fence.svg">
      <monkey class="jump e4" style="position: absolute; bottom:0; left:7%;"><img src="./assets/images/monkey.svg">
      <crown class="jump e5"  style="position: absolute; top:-17%; left:19%;"><img src="./assets/images/crown.svg"></crown></monkey>
      </fence>
      <fence class="jump e6"><img src="./assets/images/fence.svg">
      <monkey class="jump e7 baby" style="position: absolute; bottom:0; left:22%;"><img src="./assets/images/monkey.svg"></monkey>
      </fence>
      <sheep class="jump e8 baby"><img src="./assets/images/sheep.svg"></sheep>`,
    answer: '1 2 3 4 5 6 7 8',
    help: '*',
    html: `<div class="e1">&lt;cage&gt;</div>
      <div class="e2 t1">&lt;monkey class="baby"&gt;&lt;/monkey&gt;</div>
      <div class="e1">&lt;/cage&gt;</div>
      <div class="e3">&lt;fence&gt;</div>
      <div class="e4 t1">&lt;monkey&gt;</div>
      <div class="e5 t1">&lt;crown&gt;&lt;/crown&gt;</div>
      <div class="e4 t1">&lt;/monkey&gt;</div>
      <div class="e3">&lt;/fence&gt;</div>
      <div class="e6">&lt;fence&gt;</div>
      <div class="e7 t2">&lt;monkey class="baby"&gt;&lt;/monkey&gt;</div>
      <div class="e6">&lt;/fence&gt;</div>
      <div class="e8">&lt;sheep class="baby"&gt;&lt;/sheep&gt;</div>`,
    desc: 'Select everything.<br>Hint: use universal selector (asterisk)',
  },
  // клетка (бэбиманки), овца, клетка (бэбиовца), забор (манки)
  10: {
    display: `<cage class="e1"><img src="./assets/images/littlecage.svg">
      <monkey class="jump e2 baby" style="position: absolute; bottom:0; left:22%;"><img src="./assets/images/monkey.svg"></monkey>
      </cage>
      <sheep class="e3"><img src="./assets/images/sheep.svg"></sheep>
      <cage class="e4"><img src="./assets/images/littlecage.svg">
      <sheep class="jump e5 baby" style="position: absolute; bottom:0; left:5%;"><img src="./assets/images/sheep.svg"></sheep>
      </cage>
      <fence class="e6"><img src="./assets/images/fence.svg">
      <monkey class="e7" style="position: absolute; bottom:0; left:7%;"><img src="./assets/images/monkey.svg"></monkey>
      </fence>`,
    answer: '2 5',
    help: 'cage *',
    html: `<div class="e1">&lt;cage&gt;</div>
      <div class="e2 t1">&lt;monkey class="baby"&gt;&lt;/monkey&gt;</div>
      <div class="e1">&lt;/cage&gt;</div>
      <div class="e3">&lt;sheep class="baby"&gt;&lt;/sheep&gt;</div>
      <div class="e4">&lt;cage&gt;</div>
      <div class="e5 t1">&lt;sheep class="baby"&gt;&lt;/sheep&gt;</div>
      <div class="e4">&lt;/cage&gt;</div>
      <div class="e6">&lt;fence&gt;</div>
      <div class="e7 t1">&lt;monkey&gt;&lt;/monkey&gt;</div>
      <div class="e6">&lt;/fence&gt;</div>`,
    desc: 'Select all animals from the cages.<br>Hint: use tag and universal selector combination',
  },
  // манки, клетка, бэбиманки, клетка, манки
  11: {
    display: `<monkey class="e1"><img src="./assets/images/monkey.svg"></monkey>
      <cage class="e2"><img src="./assets/images/littlecage.svg"></cage>
      <monkey class="jump e3 baby"><img src="./assets/images/monkey.svg"></monkey>
      <cage class="e4"><img src="./assets/images/littlecage.svg"></cage>
      <monkey class="jump e5"><img src="./assets/images/monkey.svg"></monkey>`,
    answer: '3 5',
    help: 'cage + monkey',
    html: `<div class="e1">&lt;monkey&gt;&lt;/monkey&gt;</div>
      <div class="e2">&lt;cage&gt;&lt;/cage&gt;</div>
      <div class="e3">&lt;monkey class="baby"&gt;&lt;/monkey&gt;</div>
      <div class="e4">&lt;cage&gt;&lt;/cage&gt;</div>
      <div class="e5">&lt;monkey&gt;&lt;/monkey&gt;</div>`,
    desc: 'Select monkeys next to cages.<br>Hint: use adjacent sibling selector (plus sign)',
  },
  // забор, манки, бэбиманки, клетка(бэбиманки)
  12: {
    display: `<fence class="e1"><img src="./assets/images/fence.svg"></fence>
      <monkey class="jump e2"><img src="./assets/images/monkey.svg"></monkey>
      <monkey class="jump e3 baby"><img src="./assets/images/monkey.svg"></monkey>
      <cage class="e4"><img src="./assets/images/littlecage.svg">
      <monkey class="e5 baby" style="position: absolute; bottom:0; left:22%;"><img src="./assets/images/monkey.svg"></monkey></cage>`,
    answer: '2 3',
    help: 'fence ~ monkey',
    html: `<div class="e1">&lt;fence&gt;&lt;/fence&gt;</div>
      <div class="e2">&lt;monkey&gt;&lt;/monkey&gt;</div>
      <div class="e3">&lt;monkey class="baby"&gt;&lt;/monkey&gt;</div>
      <div class="e4">&lt;cage&gt;</div>
      <div class="e5 t1">&lt;monkey class="baby"&gt;&lt;/monkey&gt;</div>
      <div class="e4">&lt;/cage&gt;</div>`,
    desc: 'Select the monkeys beside fence.<br>Hint: use general sibling selector (tilda)',
  },
  // камень(манки(корона)), камень(корона)
  13: {
    display: `
      <stone class="e1"><img src="./assets/images/stone.svg">
      <crown class="e2 jump " style="position: absolute; bottom:0; top:-100%; left:22%;"><img src="./assets/images/crown.svg"></crown></stone>
      <stone class="e3"><img src="./assets/images/stone.svg">
      <monkey class="e4" style="position: absolute; bottom:55%; left:7%;"><img src="./assets/images/monkey.svg">
      <crown class="e5 t1" style="position: absolute; top:-17%; left:19%;"><img src="./assets/images/crown.svg"></crown>
      </monkey></stone>`,
    answer: '2',
    help: 'stone > crown',
    html: `<div class="e1">&lt;stone&gt;</div>
      <div class="e2 t1">&lt;crown&gt;&lt;/crown&gt;</div>
      <div class="e1">&lt;/stone&gt;</div>
      <div class="e3">&lt;stone&gt;</div>
      <div class="e4 t1">&lt;monkey&gt;</div>
      <div class="e5 t2">&lt;crown&gt;&lt;/crown&gt;</div>
      <div class="e4 t1">&lt;/monkey&gt;</div>
      <div class="e3">&lt;/stone&gt;</div>`,
    desc: 'Select the crown directly on the stone.<br>Hint: use child selector (greater sign)',
  },
  // камень(бэбиманки, бэбиманки) камень(бэбиовца, бэбиманки)
  14: {
    display: `<stone class="e1" style="width:100px; top: -7%;">
      <monkey class="jump e2 baby" style="position: absolute; bottom:50%; left:6%;"><img src="./assets/images/monkey.svg"></monkey>
      <monkey class="e3 baby" style="position: absolute; bottom:50%; left:55%;"><img src="./assets/images/monkey.svg"></monkey>
      <img src="./assets/images/stone.svg">
      </stone>
      <stone class="e4" style="width:100px; top: -7%;">
      <sheep class="e5 baby jump" style="position: absolute; bottom:50%; left:-10%;"><img src="./assets/images/sheep.svg"></sheep>
      <monkey class="e6 baby" style="position: absolute; bottom:50%; left:56%;"><img src="./assets/images/monkey.svg"></monkey>
      <img src="./assets/images/stone.svg">
      </stone>`,
    answer: '2 5',
    help: 'stone *:first-child',
    html: `<div class="e1">&lt;stone&gt;</div>
      <div class="e2 t1">&lt;monkey class="baby"&gt;&lt;/monkey&gt;</div>
      <div class="e3 t1">&lt;monkey class="baby"&gt;&lt;/monkey&gt;</div>
      <div class="e1">&lt;/stone&gt;</div>
      <div class="e4">&lt;stone&gt;</div>
      <div class="e5 t1">&lt;sheep class="baby"&gt;&lt;/sheep&gt;</div>
      <div class="e6 t1">&lt;monkey class="baby"&gt;&lt;/monkey&gt;</div>
      <div class="e4">&lt;/stone&gt;</div>`,
    desc: 'Select the first animals on the stones.<br>Hint: use :first-child pseudo-selector',
  },
  // камень(бэбиманки, бэбиманки) камень(бэбиовца, бэбиманки)
  15: {
    display: `<stone class="e1" style="width:100px; top: -7%;"><img src="./assets/images/stone.svg">
      <monkey class="e2 baby" style="position: absolute; bottom:50%; left:6%;"><img src="./assets/images/monkey.svg"></monkey>
      <monkey class="e3 baby jump" style="position: absolute; bottom:50%; left:55%;"><img src="./assets/images/monkey.svg"></monkey>
      </stone>
      <stone class="e4" style="width:100px; top: -7%;"><img src="./assets/images/stone.svg">
      <sheep class="e5 baby" style="position: absolute; bottom:50%; left:-10%;"><img src="./assets/images/sheep.svg"></sheep>
      <monkey class="e6 baby jump" style="position: absolute; bottom:50%; left:56%;"><img src="./assets/images/monkey.svg"></monkey>
      </stone>`,
    answer: '3 6',
    help: 'stone *:last-child',
    html: `<div class="e1">&lt;stone&gt;</div>
      <div class="e2 t1">&lt;monkey class="baby"&gt;&lt;/monkey&gt;</div>
      <div class="e3 t1">&lt;monkey class="baby"&gt;&lt;/monkey&gt;</div>
      <div class="e1">&lt;/stone&gt;</div>
      <div class="e4">&lt;stone&gt;</div>
      <div class="e5 t1">&lt;sheep class="baby"&gt;&lt;/sheep&gt;</div>
      <div class="e6 t1">&lt;monkey class="baby"&gt;&lt;/monkey&gt;</div>
      <div class="e4">&lt;/stone&gt;</div>`,
    desc: 'Select the last animals on the stones.<br>Hint: use :last-child pseudo-selector',
  },
  // манки манки манки манки
  16: {
    display: `<monkey class="e1"><img src="./assets/images/monkey.svg"></monkey>
      <monkey class="e2" id="rambo"><img src="./assets/images/rambomonkey.svg"></monkey>
      <monkey class="jump e3"><img src="./assets/images/monkey.svg"></monkey>
      <monkey class="e4"><img src="./assets/images/monkey.svg"></monkey>`,
    answer: '3',
    help: 'monkey:nth-child(3)',
    html: `
      <div class="e1">&lt;monkey"&gt;&lt;/monkey&gt;</div>
      <div class="e2">&lt;monkey id="rambo"&gt;&lt;/monkey&gt;</div>
      <div class="e3">&lt;monkey"&gt;&lt;/monkey&gt;</div>
      <div class="e4">&lt;monkey"&gt;&lt;/monkey&gt;</div>`,
    desc: 'Select the third monkey.<br>Hint: use :nth-child() pseudo-selector',
  },
  // бэбиовца, бэбиовца, бэбиовца, бэбиовца
  17: {
    display: `<sheep class="e1 baby"><img src="./assets/images/sheep.svg"></sheep>
      <sheep class="e2 baby"><img src="./assets/images/sheep.svg"></sheep>
      <sheep class="jump e3 baby"><img src="./assets/images/sheep.svg"></sheep>
      <sheep class="e4 baby"><img src="./assets/images/sheep.svg"></sheep>`,
    answer: '3',
    help: 'sheep:nth-last-child(2)',
    html: `
      <div class="e1">&lt;sheep&gt;&lt;/sheep&gt;</div>
      <div class="e2">&lt;sheep&gt;&lt;/sheep&gt;</div>
      <div class="e3">&lt;sheep&gt;&lt;/sheep&gt;</div>
      <div class="e4">&lt;sheep&gt;&lt;/sheep&gt;</div>`,
    desc: 'Select the second sheep from the end.<br>Hint: use :nth-last-child() pseudo-selector',
  },
  // бэбишип манки бэбиманки шип
  18: {
    display: `<sheep class="e1 baby"><img src="./assets/images/sheep.svg"></sheep>
      <monkey class="jump e2"><img src="./assets/images/monkey.svg"></monkey>
      <monkey class="e3 baby"><img src="./assets/images/monkey.svg"></monkey>
      <sheep class="e4"><img src="./assets/images/sheep.svg"></sheep>`,
    answer: '2',
    help: 'monkey:first-of-type',
    html: `
    <div class="e1">&lt;sheep class="baby"&gt;&lt;/sheep&gt;</div>
    <div class="e2">&lt;monkey&gt;&lt;/monkey&gt;</div>
    <div class="e3">&lt;monkey class="baby"&gt;&lt;/monkey&gt;</div>
    <div class="e4">&lt;sheep&gt;&lt;/sheep&gt;</div>`,
    desc: 'Select the first monkey.<br>Hint: use :first-of-type pseudo-selector',
  },
  19: {
    display: `<sheep class="jump e1 baby"><img src="./assets/images/sheep.svg"></sheep>
      <sheep class="e2 baby"><img src="./assets/images/sheep.svg"></sheep>
      <sheep class="jump e3 baby"><img src="./assets/images/sheep.svg"></sheep>
      <sheep class="e4 baby"><img src="./assets/images/sheep.svg"></sheep>
      <sheep class="jump e5 baby"><img src="./assets/images/sheep.svg"></sheep>`,
    answer: '1 3 5',
    help: 'sheep:nth-of-type(odd)',
    html: `
      <div class="e1">&lt;sheep&gt;&lt;/sheep&gt;</div>
      <div class="e2">&lt;sheep&gt;&lt;/sheep&gt;</div>
      <div class="e3">&lt;sheep&gt;&lt;/sheep&gt;</div>
      <div class="e4">&lt;sheep&gt;&lt;/sheep&gt;</div>
      <div class="e5">&lt;sheep&gt;&lt;/sheep&gt;</div>`,
    desc: 'Select all odd sheeps.<br>Hint: use :nth-of-type() pseudo-selector',
  },
  20: {
    display: `<monkey class="e1 jump"><img src="./assets/images/monkey.svg"></monkey>
      <monkey class="e2" id="rambo"><img src="./assets/images/rambomonkey.svg"></monkey>
      <monkey class="jump e3"><img src="./assets/images/monkey.svg"></monkey>
      <monkey class="e4 jump"><img src="./assets/images/monkey.svg"></monkey>`,
    answer: '1 3 4',
    help: ':not(#rambo)',
    html: `
      <div class="e1">&lt;monkey&gt;&lt;/monkey&gt;</div>
      <div class="e2">&lt;monkey id="rambo"&gt;&lt;/monkey&gt;</div>
      <div class="e3">&lt;monkey&gt;&lt;/monkey&gt;</div>
      <div class="e4">&lt;monkey&gt;&lt;/monkey&gt;</div>`,
    desc: 'Select all monkeys except Rambo.<br>Hint: use :not() pseudo-selector',
  },
};
