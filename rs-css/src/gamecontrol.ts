import storage from './storage';
// eslint-disable-next-line import/no-cycle
import { Events, EventFunc } from './game';

enum LevelStatus {
  NOT_SOLVED,
  SOLVED,
  CHEATED,
}

export default class GameControl {
  private readonly raiseEvent;
  private readonly root: Element;
  private levels: Array<any>;

  constructor(eventFunction: EventFunc, levelsNumber: number) {
    this.raiseEvent = eventFunction;
    this.root = document.querySelector('#levels');

    this.levels = storage.load('levels') || [];

    this.init(levelsNumber);
  }

  init(levelsNumber: number): void {
    if (this.levels.length === 0) {
      this.levelsInit(levelsNumber);
      storage.save('levels', this.levels);
    }

    const levelStatus = ['<span class="level_check_not_solved">✓</span>',
      '<span class="level_check_solved">✓</span>',
      '<span class="level_check_cheated">✓</span>'];
    for (let i = 1; i < this.levels.length; i += 1) {
      this.root.insertAdjacentHTML('beforeend', `<h2>${levelStatus[this.levels[i].status]} ${i}</h2>`);
    }

    this.root.insertAdjacentHTML('afterbegin', '<button class="level_button_help">HELP!!!</button>');
    this.root.insertAdjacentHTML('beforeend', '<button class="level_button_reset">Reset progress</button>');
    this.root.querySelectorAll('h2')[this.levels[0].curLevel - 1].classList.add('level_active');
    this.changeLevel(this.levels[0].curLevel);

    // Hook Events
    this.root.querySelectorAll('h2').forEach((el) => el.addEventListener('click', (e) => {
      this.changeLevel(+(e.currentTarget as HTMLElement).textContent.slice(2));
    }));
    this.root.querySelector('.level_button_help').addEventListener('click',
      () => this.raiseEvent.bind(this)('getHelp', { level: this.levels[0].curLevel }));
    this.root.querySelector('.level_button_reset').addEventListener('click', this.resetLevels.bind(this));
  }

  changeLevel(level: number): void {
    this.levels[0].curLevel = level;
    storage.save('levels', this.levels);
    this.root.querySelector('.level_active').classList.remove('level_active');
    this.root.querySelectorAll('h2')[level - 1].classList.add('level_active');
    this.raiseEvent(Events.START_LEVEL, { level });
  }

  levelsInit(levelsNumber: number): void {
    this.levels.length = levelsNumber + 1;
    for (let i = 1; i < this.levels.length; i += 1) {
      this.levels[i] = { status: 0 };
    }
    this.levels[0] = { curLevel: 1 };
  }

  help():void {
    if (this.levels[this.levels[0].curLevel].status === LevelStatus.NOT_SOLVED) {
      this.levels[this.levels[0].curLevel].status = LevelStatus.CHEATED;
      this.root.querySelectorAll('h2 span')[this.levels[0].curLevel - 1].className = 'level_check_cheated';
    }
    storage.save('levels', this.levels);
  }

  markSolved():void {
    if (this.levels[this.levels[0].curLevel].status !== LevelStatus.CHEATED) {
      this.levels[this.levels[0].curLevel].status = LevelStatus.SOLVED;
      this.root.querySelectorAll('h2 span')[this.levels[0].curLevel - 1].className = 'level_check_solved';
    }
    storage.save('levels', this.levels);
  }

  getCurrentLevel(): number {
    return this.levels[0].curLevel;
  }

  private resetLevels(): void {
    this.levelsInit(this.levels.length - 1);
    storage.save('levels', this.levels);
    this.changeLevel(1);

    this.root.querySelectorAll('h2 span').forEach((el) => {
      const el1 = el;
      el1.className = 'level_check_not_solved';
    });
  }
}
