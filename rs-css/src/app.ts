import Game from './game';
import './styles.scss';
import './assets/images/rs_school.svg';
import './assets/images/monkey.svg';
import './assets/images/rambomonkey.svg';
import './assets/images/crown.svg';
import './assets/images/sheep.svg';
import './assets/images/littlecage.svg';
import './assets/images/fence.svg';
import './assets/images/stone.svg';
import './assets/images/close.svg';

window.onload = () => {
  const game = new Game();
  game.start();
};
