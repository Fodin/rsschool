// eslint-disable-next-line import/no-cycle
import Display from './display';
import LevelsStorage from './levelstorage';
// eslint-disable-next-line import/no-cycle
import HTMLView from './htmlview';
import CSSInput from './cssinput';
// eslint-disable-next-line import/no-cycle
import GameControl from './gamecontrol';

const ANIMATION_INTERVAL = 200; // Interval between appearing of chars in printing help

export interface Params {
  level?: number,
  answer?: string,
  class?: string,
}

export type EventFunc = (message: Events, params: Params) => void;

export enum Events {
  START_LEVEL,
  GET_HELP,
  SWITCH_TOOLTIP,
  CHECK_ANSWER,
}

export default class Game {
  private readonly display: Display;
  private readonly levelStorage: LevelsStorage;
  private readonly htmlView: HTMLView;
  private readonly cssInput: CSSInput;
  private readonly gameControl: GameControl;

  constructor() {
    const eventFunction: EventFunc = this.processEvent.bind(this);
    this.display = new Display(eventFunction);
    this.levelStorage = new LevelsStorage();
    this.htmlView = new HTMLView(eventFunction);
    this.cssInput = new CSSInput(eventFunction);
    this.gameControl = new GameControl(eventFunction, this.levelStorage.levelsNumber);
  }

  start(): void {
    console.log('start', this.display);
  }

  processEvent(message : Events, params): void {
    switch (message) {
      case Events.START_LEVEL: {
        this.startLevel(params.level);
        break;
      }
      case Events.GET_HELP: {
        this.getHelp(params.level);
        break;
      }
      case Events.SWITCH_TOOLTIP: {
        this.display.tooltip(params.class, this.htmlView.getLine(params.class));
        this.htmlView.highlightLines(params.class);
        break;
      }
      case Events.CHECK_ANSWER: {
        this.checkAnswer(params.answer);
        break;
      }
      default: break;
    }
  }

  private startLevel(level: number) {
    this.htmlView.setHtml(this.levelStorage.getLevelHTML(level));
    this.display.setHtml(this.levelStorage.getLevelDisplayHTML(level));
    this.cssInput.setCss(this.levelStorage.getLevelDescription(level));
  }

  private getHelp(level: number) {
    const help: string = this.levelStorage.getHelp(level);
    this.cssInput.setAnswer(help, ANIMATION_INTERVAL);
    this.gameControl.help();
  }

  private checkAnswer(answer: string) {
    const sortedAnswers: string[] = this.display.getSelectorResult(answer).sort();
    const curLevel: number = this.gameControl.getCurrentLevel();
    const isRightAnswer: boolean = sortedAnswers.join(' ') === this.levelStorage.getAnswer(curLevel);

    if (!isRightAnswer) {
      this.display.animateWrong(sortedAnswers.map((el) => `.e${el}>img`).join(', '));
    } else {
      this.display.animateRight(sortedAnswers.map((el) => `.e${el}>img`).join(', '));
      this.gameControl.markSolved();

      setTimeout(() => {
        if (this.levelStorage.levelsNumber === curLevel) {
          document.querySelector('dialog').showModal();
          this.gameControl.changeLevel(curLevel);
        } else {
          this.gameControl.changeLevel(curLevel + 1);
        }
      }, 2000);
    }
  }
}
