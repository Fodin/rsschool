import hljs from 'highlight.js';
import './assets/styles/idea.css';
// eslint-disable-next-line import/no-cycle
import { EventFunc, Events } from './game';

export default class HTMLView {
  private readonly raiseEvent: EventFunc;
  private readonly root: HTMLElement;
  private highlightClass: string;

  constructor(eventFunction: EventFunc) {
    this.raiseEvent = eventFunction;
    this.root = document.querySelector('#htmlview');

    this.initDOM();
  }

  initDOM(): void {
    // Set eventlisteners
    this.root.addEventListener('mouseover', (ev) => {
      let el = ev.target as HTMLElement;
      while (el.tagName !== 'DIV') {
        el = el.parentElement;
      }
      const result: RegExpMatchArray = el.className.match(/e\d+/);
      if (result === null) {
        if (this.highlightClass === '') {
          return;
        }
        this.highlightClass = '';
      } else if (result[0] === this.highlightClass) {
        return;
      } else {
        [this.highlightClass] = result;
      }
      this.raiseEvent(Events.SWITCH_TOOLTIP, { class: this.highlightClass });
    });

    this.root.addEventListener('mouseleave', () => {
      if (this.highlightClass === '') {
        return;
      }
      this.highlightClass = '';
      this.raiseEvent(Events.SWITCH_TOOLTIP, { class: this.highlightClass });
    });
  }

  setHtml(levelHtml: string): void {
    this.root.innerHTML = levelHtml;
    hljs.highlightBlock(this.root);
    this.highlightClass = '';
  }

  highlightLines(highlightClass: string): void {
    this.root.querySelectorAll('.highlight-line').forEach((el) => {
      el.classList.remove('highlight-line');
    });
    if (highlightClass === '') return;

    this.root.querySelectorAll(`.${highlightClass}`).forEach((el) => {
      el.classList.add('highlight-line');
    });
  }

  getLine(className: string): string {
    if (className === '') {
      return '';
    }

    let line: string = '';
    this.root.querySelectorAll(`.${className}`).forEach((el) => {
      line = line.concat(el.textContent);
    });
    return line;
  }
}
